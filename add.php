<?php
	include 'layout.html';
	include 'validatemail.php';
	include 'dbinfo.php';
	
	
	//validation vars
	$search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                 "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
                 "'&(quot|#34);'i",                // Replace HTML entities
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(d+);'e");                    // evaluate as php

	$replace = array ("",
                 "",
                 "\1",
                 "\"",
                 "&",
                 "<",
                 ">",
                 " ",
                 chr(161),
                 chr(162),
                 chr(163),
                 chr(169),
                 "chr(\1)");
    
    //Get data
    $nombre=$_POST["nombre"];
	$email=$_POST["email"];
	$titulo=$_POST["titulo"];
	$idea=$_POST["idea"];
	
	if(!$nombre || !$email || !$titulo || !$idea){
		echo "<h3>Al parecer olvidas ingresar algún dato</h3>";
		?>
			<p>Por favor, revisa los datos y vuelve a intentar. </p><a href="javascript:history.go(-1)" class="btn">Volver</a>
		<?php
		die();
	}
	if(!isEmail($email)){
		echo "<h3>Ouch!</h3>";
		?>
			<p>Al parecer el email ingresado no es válido. Intenta nuevamente </p><a href="javascript:history.go(-1)" class="btn">Volver</a>
		<?php
		die();
	}

	//Reformat data
	
	$nombre = preg_replace($search, $replace, $nombre);
	$email = preg_replace($search, $replace, $email);
	$titulo = preg_replace($search, $replace, $titulo);
	$idea = preg_replace($search, $replace, $idea);
	
	$link =  mysql_connect($host, $mysql_user, $mysql_password);
	if (!$link) {
    	die('No pude conectarme: ' . mysql_error());
	}
	
	$sql = "INSERT INTO $mysql_db.idea (id,nombre_persona,email,titulo_idea,cuerpo_idea,fecha_de_incorporacion,aprobada) VALUES (NULL, '$nombre', '$email', '$titulo', '$idea', CURRENT_TIMESTAMP, '0');";
	$execute=mysql_query($sql);
	if($execute){
                $para = $admin_mail;
			$titulo = 'Nueva Idea en 1000 Ideas';
			$mensaje = '
			<html>
			<head>
  			<title>Hay Nuevas Ideas Pendientes de Moderación</title>
			</head>
			<body>
			<h1>Una Nueva Idea Espera!</h1>
			<h3>Hola, Administrador!</h3>
			<p>Hay nuevas ideas pendientes de moderación en 1000 Ideas.</p>
			<p>Para ir al panel de moderación, haz click en el siguiente link: <a href="http://<?php echo $path; ?>/admin">http://<?php echo $path; ?>/admin</a> </p>
			<p>&nbsp;</p>
			<p>Atte,</p>
			<h4>-El equipo de 1000 Ideas.</h4>
			</body>
			</html>
			';
			
			$cabeceras = 'Content-type: text/html; charset=utf-8' . "\r\n".
			'MIME-Version: 1.0' . "\r\n".
			'From: 1000 Ideas <no-reply@mail.com>' . "\r\n" .
   			'Reply-To: reply@mail.com' . "\r\n" .
   			'X-Mailer: PHP/' . phpversion();
			mail($para, $titulo, $mensaje, $cabeceras);
		echo "<h1>Gracias!</h1>";
		echo "<p>Tu idea será publicada dentro de poco tiempo. Te enviaremos un mail con el link cuando esté aprobada.</p>";
		?>
		
		<p>Mientras llega el mail, puedes ver las últimas ideas recibidas!</p>
		<a href="index.php" class="btn btn-success">Ver</a>
		
		<?php
		
	}
	include 'footer.html';
	
?>