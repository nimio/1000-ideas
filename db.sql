-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2012 at 11:53 AM
-- Server version: 5.5.22
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `arivas`
--

-- --------------------------------------------------------

--
-- Table structure for table `idea`
--

CREATE TABLE IF NOT EXISTS `idea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_persona` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `titulo_idea` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cuerpo_idea` varchar(5000) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_de_incorporacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `aprobada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=55 ;

--
-- Dumping data for table `idea`
--

INSERT INTO `idea` (`id`, `nombre_persona`, `email`, `titulo_idea`, `cuerpo_idea`, `fecha_de_incorporacion`, `aprobada`) VALUES
(50, 'Angelo Cares', 'angelo@artkind.net', 'La mejor idea del mundo', 'Lorem ipsum dolor sit amet, coectetur adipiscing elit. Sed in mauris quam. Proin adipiscing est non orci luctus adipiscing interdum purus vulputate. Nullam nec varius sem. Lorem ipsum dolor sit amet, coectetur adipiscing elit. Phasellus at massa nisi. Nunc laoreet neque nec ligula gravida viverra. Nunc sollicitudin gravida ipsum, tempor placerat nulla facilisis sit amet. Etiam rhoncus adipiscing dui sed iaculis. Nulla eget leo tellus. Suspendisse tellus mi, commodo eu aliquet et, pellentesque sagittis orci. In augue nulla, sodales at vestibulum non, pretium ac arcu. Aenean coequat convallis sem, in tempor metus luctus ac. Praesent vel enim a metus ullamcorper aliquam vel vitae dui. In sed erat at felis molestie pharetra a at nisl. Maecenas commodo lacus viverra augue varius at iaculis mauris condimentum.', '2012-03-27 20:28:30', 1),
(51, 'Angelo Cares', 'me@angelocares.com', 'Otra idea', 'Lorem ipsum dolor sit amet, coectetur adipiscing elit. Sed in mauris quam. Proin adipiscing est non orci luctus adipiscing interdum purus vulputate. Nullam nec varius sem. Lorem ipsum dolor sit amet, coectetur adipiscing elit. Phasellus at massa nisi. Nunc laoreet neque nec ligula gravida viverra. Nunc sollicitudin gravida ipsum, tempor placerat nulla facilisis sit amet. Etiam rhoncus adipiscing dui sed iaculis. Nulla eget leo tellus. Suspendisse tellus mi, commodo eu aliquet et, pellentesque sagittis orci. In augue nulla, sodales at vestibulum non, pretium ac arcu. Aenean coequat convallis sem, in tempor metus luctus ac. Praesent vel enim a metus ullamcorper aliquam vel vitae dui. In sed erat at felis molestie pharetra a at nisl. Maecenas commodo lacus viverra augue varius at iaculis mauris condimentum.', '2012-03-27 20:42:26', 1),
(53, 'Angelo Cares', 'me@angelocares.com', 'Mi gran idea', 'wjhvdwjhebvw weud weiudw exiow eodubwe eoiwed woeiud.', '2012-03-27 22:15:53', 1),
(54, 'Angelo Cares', 'me@angelocares.com', 'Idea de Prueba', 'ijdsbis sodiuf osd sod sodjk fsodjk.', '2012-03-27 22:34:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'acares', 'b684f9bf60e7a647137212bacab12038'),
(2, 'jcpantoja', '39e42175ad5d5d33bcc75a46adf46487');

-- --------------------------------------------------------

--
-- Table structure for table `votos_idea`
--

CREATE TABLE IF NOT EXISTS `votos_idea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_idea` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=129 ;

--
-- Dumping data for table `votos_idea`
--

INSERT INTO `votos_idea` (`id`, `id_idea`, `fecha`, `ip`) VALUES
(120, 50, '2012-03-27 20:28:30', '0.0.0.0'),
(121, 50, '2012-03-27 20:41:36', '127.0.0.1'),
(122, 51, '2012-03-27 20:42:26', '0.0.0.0'),
(123, 50, '2012-03-27 22:12:40', '186.9.129.248'),
(124, 53, '2012-03-27 22:15:53', '0.0.0.0'),
(125, 51, '2012-03-27 22:16:44', '186.9.129.248'),
(126, 50, '2012-03-27 22:32:33', '186.9.129.248'),
(127, 54, '2012-03-27 22:34:13', '0.0.0.0'),
(128, 54, '2012-03-27 22:36:24', '186.9.129.248');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
