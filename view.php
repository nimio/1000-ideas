<?php
	$id=$_GET["idea"];
	include 'dbinfo.php';
        
	
	$link =  mysql_connect($host, $mysql_user, $mysql_password);
	if (!$link) {
    	die('No pude conectarme: ' . mysql_error());
	}
	
	
	
	$sql = "SELECT * FROM $mysql_db.idea WHERE id=$id;";
	$count = "SELECT id_idea FROM $mysql_db.votos_idea WHERE id_idea=$id;";
	$result=mysql_query($sql);
	$result_count=mysql_query($count);
	$votos = mysql_num_rows($result_count);
	$row = mysql_fetch_array($result, MYSQL_ASSOC);
        $num_rows = mysql_numrows($result);
        ?>
        <!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- meta facebook -->
        <meta property="og:type"  content="[Poner Tipo]" />
        <meta property="og:site_name" content="1000 Ideas" />
        <meta property="og:title" content="1000 Ideas" />
        <meta property="og:url"   content="http://nim.io/" />
	<meta property="og:image" content="http://nim.io/logo.png" />
        <meta property="og:description" content="Todos tenemos ideas que pueden ayudar a mejorar [lo que sea] y aquí son escuchadas (o leídas). Si tu también tienes una idea, envíala y difúndela en las redes sociales!" />
        
        <title>1000 ideas | <?php echo $row["titulo_idea"]; ?></title>
        
	<link rel="stylesheet" href="static/css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" href="static/css/style.css" type="text/css" media="screen">
	<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>	
	<script type="text/javascript" src="static/js/jquery.js"></script>
	<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
</head>
<body>
	 <div id="wrap">
	     <div id="fb-root"></div>
			<script>/*Facebook init here*/</script>
	 	<div id="header">
	 		<div id="logo"><a href="/"><img src="static/img/logo.png" alt=""></a></div>
	 	</div>
	 	
	<div id="main-content">
		<div class="row-fluid">
        <?php

        if($num_rows==0){
        ?>
        <div class="alert">
             <h3>Ouch!</h3>
             <p>La idea que buscas no existe.</p>
             <a href="/1000ideas" class="btn btn-success">Encuéntrala aquí!</a>
        </div>
        <?php
        die();
        }
    ?>

    			<ul class="breadcrumb">
  						<li>
    						<a href="/">Inicio</a> <span class="divider">/</span>
  						</li>
  						<li>
    						<a href="/1000ideas">1000 Ideas</a> <span class="divider">/</span>
  						</li>
  						<li class="active"><?php echo $row["titulo_idea"]; ?></li>
				</ul>
    			<div class="span9 well" style="margin-left:0;">
    			<h1 class="span12" style="margin-left:0; text-transform: uppercase;"><?php echo $row["titulo_idea"]; ?></h1>
    			<div class="span12" style="margin-left:0;"><span class="label label-warning"><?php echo $votos-1; ?> votos</span></div>
    			<div class="span12" style="margin-left:0;">
    				<h4 style="text-transform: capitalize;">Enviada por: <small><?php echo $row["nombre_persona"]; ?></small></h4>
    				<p></p>
    			</div>
    			
    			<div class="wp-caption span12" style="margin-left:0;">
    			 	<!-- post body -->
    				<section class="bodytext">
    				<p id="bodytext" style="text-align:justify; margin-left:0;"><?php echo $row["cuerpo_idea"]; ?></p>
    				</section>
    				<!-- end post body -->
    			</div>
    			<div class="span5" style="margin-left:0;">
    			<div class="fb-like" data-href="http://<?php echo $path; ?>/view.php?idea=<?php echo $row["id"]; ?>" data-send="true" data-layout="button_count" data-width="60" data-show-faces="true"></div>
    				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://<?php echo $path; ?>/view.php?idea=<?php echo $row["id"]; ?>" data-text="Vota por mi idea para Chiguayante!" data-lang="es">Twittear</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				
				<div class="span12" style="margin-left:0;">
    				<a href="like.php?idea=<?php echo $row["id"]; ?>" class="btn btn-primary pull-right">
    					<i class="icon-ok icon-white"></i>
    					Buena idea!
    				</a>
    			</div>
    			
    			
    			</div>
    			<div class="span12">&nbsp;</div>
    			
    			<div class="span6 well" style="margin-left:0;">
    				<h2>Comentarios</h2>
    				<div class="span12">&nbsp;</div>
    				<div class="fb-comments" data-href="http://<?php echo $path; ?>/view.php?idea=<?php echo $row["id"]; ?>" data-num-posts="10" data-width="470">
    				</div>
    			</div>

<?php include 'footer.html'; ?>