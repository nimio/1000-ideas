<?php
	include 'dbinfo.php';
	
	$link =  mysql_connect($host, $mysql_user, $mysql_password);
	if (!$link) {
    	die('No pude conectarme: ' . mysql_error());
	}
	
	$sql = "SELECT id_idea as id_idea,nombre_persona as nombre_persona,titulo_idea as titulo_idea,count(votos_idea.id_idea) as votos FROM $mysql_db.votos_idea \n"
    . "LEFT JOIN $mysql_db.idea \n"
    . "ON votos_idea.id_idea=idea.id\n"
    . "group by id_idea\n"
    . "order by votos desc\n"
    . "limit 10";
    
	$result = mysql_query($sql);
	?>
		<table class="table table-condensed">
			<tr>
				<th>#</th>
				<th>Por</th>
				<th>Titulo</th>
                                <th></th>
			</tr>
	<?php
	$count = 0;
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	$count = $count+1;
    ?>
    		<tr>
    			<td><?php echo $count; ?></td>
    			<td class="allcaps"><?php echo $row["nombre_persona"]; ?></td>
    			<td class="capitalize-first"><a href="http://<?php echo $path; ?>/view.php?idea=<?php echo $row["id_idea"]; ?>" title="Haz click para leer esta idea!"><?php echo $row["titulo_idea"]; ?></a></td>
                        <td><span class="label label-warning" style="cursor: help;" title="Esta idea tiene <?php echo $row["votos"]-1; ?> votos"><?php echo $row["votos"]-1; ?></span></td>
    		</tr>
    <?php }?>
</table>