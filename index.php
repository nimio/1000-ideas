<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- meta facebook -->
        <meta property="og:type"  content="[Poner Tipo]" />
        <meta property="og:site_name" content="1000 Ideas" />
        <meta property="og:title" content="1000 Ideas" />
        <meta property="og:url"   content="http://nim.io/" />
	<meta property="og:image" content="http://nim.io/logo.png" />
        <meta property="og:description" content="Todos tenemos ideas que pueden ayudar a mejorar [lo que sea] y aquí son escuchadas (o leídas). Si tu también tienes una idea, envíala y difúndela en las redes sociales!" />
        
        <title>1000 ideas</title>
	<link rel="stylesheet" href="static/css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" href="static/css/style.css" type="text/css" media="screen">
	<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>	
	<script type="text/javascript" src="static/js/jquery.js"></script>
	<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
</head>
<body>
	 <div id="wrap">
	     <div id="fb-root"></div>
			<script>/*Facebook init here*/</script>
	 	<div id="header">
	 		<div id="logo"><a href="/"><img src="static/img/logo.png" alt=""></a></div>
	 	</div>
	 	<div id="main-content">
		<div class="row-fluid">
<ul class="breadcrumb">
  						<li>
    						<a href="/">Inicio</a> <span class="divider">/</span>
  						</li>
  						<li>
    						<a href="/1000ideas">1000 Ideas</a> <span class="divider">/</span>
  						</li>
  						<li class="active">Todas las ideas</li>
				</ul>

<?php
	include 'dbinfo.php';
	
	$link =  mysql_connect($host, $mysql_user, $mysql_password);
	if (!$link) {
    	die('No pude conectarme: ' . mysql_error());
	}
	
	$sql = "SELECT id_idea as id_idea,nombre_persona as nombre_persona,titulo_idea as titulo_idea, cuerpo_idea as cuerpo_idea,count(votos_idea.id_idea) as votos FROM $mysql_db.votos_idea\n"
    . "\n"
    . "LEFT JOIN $mysql_db.idea\n"
    . "ON votos_idea.id_idea=idea.id\n"
    . "\n"
    . "WHERE idea.aprobada=1\n"
    . "group by id_idea\n"
    . "order by votos desc;";

	$result=mysql_query($sql);
        $aers=0;
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
	
	$body = $row["cuerpo_idea"];
	$cuerpo = substr($body,0,255);
	$votos= $row["votos"]-1;
        
    ?>
    			<div class="span9 well" style="margin-left:0;">
    			<div class="span12" style="margin-left:0;">
    			<h3 style="text-transform: uppercase;"><?php echo $row["titulo_idea"]; ?> <span class="label label-warning"><?php echo $votos; ?> votos</span></h3>
    			<br />
    			</div>
    			
    			<div class="span12" style="margin-left:0;">
    				<h4 style="text-transform: capitalize;">Enviada por: <small><?php echo $row["nombre_persona"]; ?></small></h4>
    				<p></p>
    			</div>
    			
    			<div class="span12" style="margin-left:0;" >
    				<p style="text-align:justify"><?php echo $cuerpo; ?>...</p>
    			</div>
    			<div class="span12">&nbsp;</div>
    			
    			<div class="span5" style="float:left">
    			<div class="fb-like" data-href="http://<?php echo $path; ?>/view.php?idea=<?php echo $row["id_idea"]; ?>" data-send="false" data-layout="button_count" data-width="60" data-show-faces="false"></div>
    			<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://<?php echo $path; ?>/view.php?idea=<?php echo $row["id_idea"]; ?>" data-text="Vota por mi idea para Chiguayante!" data-lang="es">Twittear</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
    			
    			<div class="span12">
    			<div class="btn-group span4" style="float:right" >
          		<a href="view.php?idea=<?php echo $row["id_idea"]; ?>" class="btn btn-success pull-right">
    					<i class="icon-zoom-in icon-white"></i>
    				Leer más
    				</a>
          		<a href="like.php?idea=<?php echo $row["id_idea"]; ?>" class="btn btn-primary pull-right">
    					<i class="icon-ok icon-white"></i>
    					Buena idea!
    				</a>
        		</div>
    			</div>
    			</div>

    <?php
if($aers==0){
?>
<div class="span3 well" style="float: right;">
<h3>¿1000 Ideas?</h3>
<p>Todos tenemos ideas que pueden ayudar a mejorar [lo que sea] y aquí son escuchadas (o leídas). Si tu también tienes una idea, envíala y difúndela en las redes sociales!.</p>
<br />
<a href="/1000ideas/new.html" class="btn btn-success pull-right">¡Tengo una Idea!</a>
</div>
<?php
$aers=1;
}
	}
?>
<?php include 'footer.html'; ?>