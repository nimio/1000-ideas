<?php
session_start();
	if(isset($_SESSION["12a023df09"])){ 
     	header("location:approve.php");
     exit; 
 	}
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>1000 ideas | Login</title>
    <meta name="description" content="1000 ideas para mi comuna">
    <meta name="author" content="ArtKind WebMedia">
    <link rel="stylesheet" href="static/css/bootstrap.css" type="text/css" media="all" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.css" type="text/css" media="all" />
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
  </head>
  <body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid">
        <a class="brand" href="index.php">NIMIO 1000Ideas <span class="label label-info">Beta</span></a>
      </div>
    </div><!-- /navbar-inner -->
  </div><!-- /navbar -->
  <br />
  <br />
  <br />
  
  <div class="container">
  <div class="span4">
  	&nbsp;
  </div>
  <div class="span3 well">
  <h1>Iniciar Sesión</h1>
  <br />
  <p>Para ingresar debes tener credenciales BTUID válidas para la versión [GIT]</p>
	<br />
<form id="1000-ideas-login" name="1000-ideas-login" action="auth.php" method="POST">
<table>
<tr>
	<td><input type="text" name="login" id="login" placeholder="Nombre de usuario"></td>
</tr>
<tr>
	<td><input type="password" name="password" id="password" placeholder="Contraseña"></td>
</tr>
<tr>
	<td><input type="submit" id="submit" name="submit" class="btn btn-success pull-right" value="Go!"></td>
</tr>
</table>
</form>
</div>
</div>
</body>
</html>