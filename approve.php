<?php
	session_start();
	if(!isset($_SESSION["12a023df09"])){ 
     	header("location:login.php?next=approve");
     exit; 
 	}
 	
	include 'dbinfo.php';
	include 'layout.html';
	
	$link =  mysql_connect($host, $mysql_user, $mysql_password);
	if (!$link) {
    	die('No pude conectarme: ' . mysql_error());
	}
	?>
        <div class="row-fluid well">
            <div class="span9">
                <h3>Aprobando Ideas</h3>
            </div>
            <div class="span3">
                <a href="logout.php" class="btn pull-right">Cerrar sesión</a>
            </div>
        <?php
	$sql = "SELECT * FROM $mysql_db.idea WHERE aprobada=0;";
	$result=mysql_query($sql);
	if(mysql_num_rows($result)==0){
		echo "<h1>Puedes descanzar!</h1>";
		echo "<br />";
		echo "<h3>No hay ideas pendientes de aprobación.</h3>";
		die();
		}
	?>
<br /><br />
	<div class="span12" style="margin-left:0;">
        <table class="table table-striped table-bordered table-condensed">
			<tr>
				<th>Nombre</th>
				<th>Email</th>
				<th>Fecha</th>
				<th>Titulo</th>
				<th>Idea</th>
				<th>Opciones</th>
				<th></th>
			</tr>
	<?php
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
    ?>
    		<tr>
    			<td><?php echo $row["nombre_persona"]; ?></td>
    			<td><?php echo $row["email"]; ?></td>
    			<td><?php echo $row["fecha_de_incorporacion"]; ?></td>
    			<td><?php echo $row["titulo_idea"]; ?></td>
    			<td><?php echo $row["cuerpo_idea"]; ?></td>
    			<td><a href="approved.php?id=<?php echo $row["id"]; ?>" class="btn btn-success">Aprobar</a></td>
    			<td><a href="drop.php?id=<?php echo $row["id"]; ?>&mail=<?php echo $row["email"]; ?>" class="btn btn-danger">Eliminar</a></td>
    		</tr>
    <?php
	}
?>
</table>
</div>
<?php include 'footer.html'; ?>